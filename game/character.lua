local Character = {}

local Character =
    new(Object) {
    sprite = nil,
    vida_maxima = nil,
    vida_atual = nil,
    energia_maxima = nil,
    energia_atual = nil,
    poder = nil,
    speed = nil,
    skills = nil,
    custo_skills = nil,
    multiplicador_skills = nil
}

function Character:init()
    self.sprite = self.sprite or nil
    self.vida_maxima = self.vida_maxima or 999
    self.vida_atual = self.vida_maxima
    self.energia_maxima = self.energia_maxima or 99
    self.energia_atual = self.energia_maxima
    self.poder = self.poder or 100
    self.speed = self.speed or 10
    self.skills = self.skills or nil
    self.custo_skills = self.custo_skills or nil
    self.multiplicador_skills = self.multiplicador_skills or nil
end

return Character

local START_ANGLE = 5 * math.pi / 8
local SIZE_ANGLE = math.pi / 3

local EnergyLeftBar =
    new "graphics.bar" {
    start_angle = START_ANGLE,
    size_angle = SIZE_ANGLE
}

return EnergyLeftBar

local ChooseAction = new "state.base" {}

function ChooseAction:onEnter(battle)
  self.graphics = battle.graphics
  self.battle = battle

  -- Cria menu de combate
  local W, H = love.graphics.getDimensions()
  local width, height = 120, 160
  local menu =
    new "graphics.menu" {
    position = new(Vec) {W / 2, 2 * H / 3},
    box = new(Box) {-width / 2, width / 2, -height / 2, height / 2}
  }
  menu:add(
    "Fight",
    function()
      self:fight()
    end
  )
  menu:add(
    "Item",
    function()
      self:item()
    end
  )
  menu:add(
    "Skills",
    function()
      self:skills()
    end
  )
  menu:add(
    "Run",
    function()
      self:run()
    end
  )
  self.graphics:add("gui", menu)
  self.menu = menu

  self.graphics:focus(menu)
end

function ChooseAction:onLeave()
  self.menu:destroy()
end

function ChooseAction:onResume()
  if self.action == "attack" and self.targets.chosen then
    self.battle:setNextAction(
      self.action,
      {
        target = self.targets.chosen,
        attacker = self.battle:currentCharacter().avatar
      }
    )
    self.stack:pop()
    self.action = nil
    self.targets = nil
  elseif self.action == "item" and self.items.chosen then
    self.battle:setNextAction(
      self.action,
      {
        target = self.battle:currentCharacter().avatar,
        item = self.items.chosen
      }
    )
    self.stack:pop()
    self.action = nil
    self.items = nil
  elseif (self.action == "activate_skill") and self.sk.chosen and self.targets.chosen then
    self.battle:setNextAction(
      self.action,
      {
        target = self.targets.chosen,
        attacker = self.battle:currentCharacter().avatar,
        skill = self.sk.chosen
      }
    )
    self.stack:pop()
    self.action = nil
    self.items = nil
    self.sk = nil
    self.targets = nil
  elseif self.action == "skill" and self.sk.chosen then
    local index = 1
    for key, value in pairs(self.battle:currentCharacter().avatar.character.skills) do
      if not value == self.sk.chosen then
        index = index + 1
      end
      if value == skill then
        break
      end
    end
    if
      (self.battle:currentCharacter().avatar.character.energia_atual >
        self.battle:currentCharacter().avatar.character.custo_skills[index])
     then
      if not self.targets then
        self.chooseTargetSkill(self)
      end
    else
      self.battle.graphics:add(
        "fx",
        new "graphics.notification" {
          position = new(Vec) {self.battle:currentCharacter().avatar.position:get()},
          color = {.2, .9, .9},
          text = "Sem enegia"
        }
      )
      -- self.graphics:focus(menu)
      self:skills()
      self.action = nil
    end
  else
    self.graphics:focus(self.menu)
  end
end

function ChooseAction:fight()
  self.action = "attack"
  local alive_enemies = {}
  for _, enemy in ipairs(self.battle.left.characters) do
    if not enemy.avatar:isDestroyed() then
      table.insert(alive_enemies, enemy)
    end
  end
  if next(alive_enemies) == nil then
    if (self.battle.enemy_parties == 1) then
      self.battle.enemy_parties = self.battle.enemy_parties + 1
      self.battle:loadParty("left", require("database.adventures.adventures")[2])
    else
      if (self.battle.enemy_parties == 2) then
        self.battle.enemy_parties = self.battle.enemy_parties + 1
        self.battle:loadParty("left", require("database.adventures.adventures")[3])
      end
    end
  else
    self.targets = alive_enemies
    self.stack:push("choose_target", self.battle, self.targets)
  end
end

function ChooseAction:item()
  self.action = "item"
  self.items = {"Potion", "Wind Crystal", "Strawberry Cake"}
  self.stack:push("choose_option", self.battle, self.items)
end

function ChooseAction:skills()
  self.action = "skill"
  self.sk = self.battle:currentCharacter().avatar.character.skills
  self.stack:push("choose_option", self.battle, self.sk)
end

function ChooseAction:chooseTargetSkill()
  self.action = "activate_skill"
  local alive_enemies = {}
  for _, enemy in ipairs(self.battle.left.characters) do
    if not enemy.avatar:isDestroyed() then
      table.insert(alive_enemies, enemy)
    end
  end
  self.targets = alive_enemies --{ unpack(self.battle.left.characters) }
  self.stack:push("choose_target", self.battle, self.targets)
end

function ChooseAction:run()
  love.event.quit()
end

return ChooseAction

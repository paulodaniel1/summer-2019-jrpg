local ExecuteAction =
  new "state.base" {
  delay = 0
}

function ExecuteAction:onEnter(battle, action)
  local name, params = action.name, action.params
  if name == "attack" then
    local target = params.target
    local attacker = params.attacker
    battle.graphics:add(
      "fx",
      new "graphics.notification" {
        position = new(Vec) {target.avatar.position:get()},
        color = {.9, .9, .2},
        text = "-" .. attacker.character.poder
      }
    )

    --Realiza o ataque no alvo
    target.avatar.character.vida_atual = target.avatar.character.vida_atual - attacker.character.poder

    --Realiza a verificação de quanta vida o alvo ainda tem para ver se ele morreu ou não
    if target.avatar.character.vida_atual <= 0 then
      target.avatar:destroy()
    end

    self.delay = 1.0
  elseif name == "item" then
    battle.graphics:add(
      "fx",
      new "graphics.notification" {
        position = new(Vec) {params.target.position:get()},
        color = {.2, .9, .9},
        text = params.item
      }
    )
  elseif name == "activate_skill" then
    local target = params.target
    local skill = params.skill
    local attacker = params.attacker

    local index = 1
    for key, value in pairs(attacker.character.skills) do
      if not value == skill then
        index = index + 1
      end
      if value == skill then
        break
      end
    end
    print(attacker.character.custo_skills[index])

    attacker.character.energia_atual = attacker.character.energia_atual - attacker.character.custo_skills[index]
    target.avatar.character.vida_atual =
      target.avatar.character.vida_atual - attacker.character.poder * attacker.character.multiplicador_skills[index]

    if target.avatar.character.vida_atual <= 0 then
      target.avatar:destroy()
    end

    -- aqui
    battle.graphics:add(
      "fx",
      new "graphics.notification" {
        position = new(Vec) {target.avatar.position:get()},
        color = {.2, .9, .9},
        text = skill
      }
    )
  end
end

function ExecuteAction:onUpdate(dt)
  self.delay = self.delay - dt
  if self.delay <= 0 then
    self.stack:pop()
  end
end

return ExecuteAction
